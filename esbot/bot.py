import discord
from discord.ext import commands
import logging
import traceback

from esbot import db

class EsBot(commands.Bot):
    def __init__(self, **kwargs) -> None:
        super().__init__()

        self.logger = logging.getLogger(__name__)
        self.load_extensions(
            'esbot.cogs.talk',
            'esbot.cogs.catharsis'
        )

    async def start(self, *args, **kwargs) -> None:
        self.db = await db.make_sqlite_conn()
        await super().start(*args, **kwargs)

    async def close(self) -> None:
        await super().close()
        await self.db.close()

    async def on_message(self, message):
        """
        This event triggers on every message received by the bot. Including one's that it sent itself.

        If you wish to have multiple event listeners they can be added in other cogs. All on_message listeners should
        always ignore bots.
        """
        # ignore all bots
        if message.author.bot:
            return
        await self.process_commands(message)

    async def on_command_error(self, context, exception):
        tb = ''.join(traceback.format_exception(type(exception), exception, exception.__traceback__))
        logging.error(f'Ignoring exception in command {context.command}:\n{tb}')
