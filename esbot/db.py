# import aioredis
# import asyncpg
# import os
import aiosqlite

async def make_sqlite_conn():
    con = await aiosqlite.connect('./data/esbot.sqlite')
    con.row_factory = aiosqlite.Row
    return con

# async def make_pg_pool():
#     return await asyncpg.create_pool()

# async def make_redis_pool(data):
#     redis = await aioredis.create_redis_pool(
#         (os.getenv('REDIS_HOST'), 6379),
#         password=os.getenv('REDIS_PASSWORD'),
#         encoding='utf8'
#     )
#     if data is not None:
#         await redis_load_dump(redis, data)
#     return redis


# async def redis_load_dump(redis, data):
#     def pipe_data_helper(key, vals, pipe=None):
#         if pipe is None:
#             pipe = redis.pipeline()

#         if isinstance(vals, dict):
#             for k, v in vals.items():
#                 _key = k if key is None else f'{key}:{k}'
#                 pipe_data_helper(_key, v, pipe)

#         elif isinstance(vals, list):
#             pipe.sadd(key, *vals)

#         elif isinstance(vals, str):
#             pipe.set(key, vals)

#         return pipe

#     pipe = pipe_data_helper(None, data)
#     await pipe.execute()
