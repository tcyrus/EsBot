import discord
from discord.ext import commands


class TalkCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def get_es_message(self, locale: str, es_angry: bool):
        es_mood = "怒り" if es_angry else "通常"

        # TODO: Import locale to db
        # if locale not in ['ja', 'ko']:
        #     locale = 'en'

        locale = 'en'

        rec = None
        async with self.bot.db.cursor() as cur:
            await cur.execute('''
                SELECT
                    CASE
                    WHEN :mood = '怒り' THEN '**' || line || '**'
                    ELSE line
                    END AS msg,
                    scenario || '-' || idx AS msg_id,
                    locale
                FROM es_talk
                WHERE locale = :locale
                AND scenario = :scenario || '-' || :mood
                ORDER BY RANDOM()
                LIMIT 1
            ''', {
                'locale': locale,
                'scenario': "4章AE",
                'mood': es_mood
            })
            rec = await cur.fetchone()
        return rec

    def gen_es_embed(self, ctx: discord.ApplicationContext, rec):
        return discord.Embed.from_dict({
            "description": rec['msg'],
            "color": 0x57ECFC,
            "fields": [
                {
                    "name": "User",
                    "value": ctx.user.mention,
                    "inline": True
                },
                {
                    "name": "Msg Id",
                    "value": rec['msg_id'],
                    "inline": True
                },
                {
                    "name": "Locale",
                    "value": rec['locale'],
                    "inline": True
                }
            ],
            "author": {
                "name": "EsBot",
                "url": ctx.me.avatar.url
            },
            "footer": {
                "text": "\N{COPYRIGHT SIGN} Caramel Column Inc. All Rights Reserved."
            }
        })

    @commands.slash_command(name="talk")
    async def talk(self, ctx: discord.ApplicationContext):
        """
        Emulates EsTalk behaviour from Game
        """

        rec = await self.get_es_message(False)
        embed = self.gen_es_embed(ctx, rec)

        await ctx.respond(embed=embed)

    @commands.slash_command(name="yell")
    async def yell(self, ctx: discord.ApplicationContext):
        """
        Emulates EsTalk behaviour from Game
        """

        rec = await self.get_es_message(ctx.locale, True)
        embed = self.gen_es_embed(ctx, rec)

        await ctx.respond(embed=embed)


def setup(bot):
    bot.add_cog(TalkCog(bot))
