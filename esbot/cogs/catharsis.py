import discord
from discord.ext import commands
from random import choice, sample

class CatharsisModal(discord.ui.Modal):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.cog = kwargs.get('cog')

        input_label = kwargs.get('input_label')
        input_placeholder = kwargs.get('input_placeholder')

        self.add_item(
            discord.ui.InputText(
                label=input_label,
                style=discord.InputTextStyle.long,
                placeholder=input_placeholder,
            )
        )

    async def callback(self, interaction: discord.Interaction):
        embed = self.cog.modal_callback(interaction, self.children[0].value)
        await interaction.response.send_message(embeds=[embed])


class CatharsisCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        # Key: [Es]診断4-1_サブタイトル
        self.modal_title = {
            'en': 'The Telling of Stories',
        }

        # Key: [Es]診断4-1_導入-10
        self.input_label = {
            'en': "So, tell me... What's bothering you?",
        }

        # Key: ThinkingArea/Placeholder
        self.input_placeholder = {
            'en': "God is in the details... of every analysis. Spare nothing."
        }

    async def get_counseling4_db(self, locale: str):
        keywordDict = {}

        # TODO: Import locale to db
        # if locale not in ['ja', 'ko']:
        #     locale = 'en'

        locale = 'en'

        rec1, rec2 = None, None
        async with self.bot.db.cursor() as cur:
            await cur.execute('''
                SELECT diagnosis, keywords
                FROM analysis_words
                WHERE locale = :locale AND
                scenario = '4-1'
            ''', {
                'locale': locale
            })
            rec1 = await cur.fetchall()

            await cur.execute('''
                SELECT diagnosis
                FROM analysis_result
                WHERE locale = :locale AND
                scenario = '4-1' AND
                instr(diagnosis, '+') > 0
            ''', {
                'locale': locale
            })
            rec2 = await cur.fetchall()

        arr2 = list(map(lambda r: r['diagnosis'], rec2))

        for record in rec1:
            text = record['diagnosis']
            keywords = record['keywords'].split("、")
            wlist = filter(lambda w: w != "", keywords)
            keywordDict[text] = list(wlist)

        return (arr2, keywordDict)

    async def get_counseling4_diag(self, data: str, arr2: list, keywordDict: dict):
        dictionary = {}
        keywords = []

        for text, wlist in keywordDict.items():
            dictionary[text] = 0
            for word in wlist:
                num = data.count(word)
                dictionary[text] += num
                if num > 0:
                    keywords.append(word)

        list2 = sorted(dictionary.items(), key=lambda kvp: kvp[1], reverse=True)
        list3, list4 = [], []

        val = list2[0][1]
        num2 = -1
        if val != 0:
            for (k, v) in list2:
                if v == val:
                    list3.append(k)
                else:
                    if num2 < 0:
                        num2 = v
                    if num2 == 0 or v != num2:
                        break
                    list4.append(j)

        text3, text4 = "その他", None
        if len(list3) > 1:
            text3, text4 = sample(list3, 2)
        elif len(list3) == 1:
            text3 = list3[0]
            if len(list4) != 0:
                text4 = choice(list4)

        if text3 == "メンヘラ" or text4 == "メンヘラ":
            text3 = "メンヘラ"
            text4 = None

        list5 = []
        if text4 is None:
            if text3 == "その他":
                list5.extend(("その他1", "その他2", "その他3",))
            else:
                list5.append(text3)
        else:
            text5 = f"{text3}+{text4}"
            text6 = f"{text4}+{text3}"
            if text5 in arr2:
                list5.append(text5)
            elif text6 in arr2:
                list5.append(text6)
            else:
                list5.extend((text3, text4,))

        result = choice(list5)
        return (result, keywords)

    async def get_counseling4_result(self, locale: str, diagnosis: str):
        rec = None

        # TODO: Import locale to db
        # if locale not in ['ja', 'ko']:
        #     locale = 'en'

        locale = 'en'

        async with self.bot.db.cursor() as cur:
            await cur.execute('''
                SELECT name, description
                FROM analysis_result
                WHERE locale = :locale AND
                scenario = '4-1' AND
                diagnosis = :diagnosis
            ''', {
                'locale': locale,
                'diagnosis': diagnosis
            })
            rec = await cur.fetchone()
        
        return rec

    def get_counseling4_embed(self, rec):
        return discord.Embed.from_dict({
            "title": rec['name'],
            "description": rec['description'],
            "color": 0x57ECFC,
            "footer": {
                "text": "\N{COPYRIGHT SIGN} Caramel Column Inc. All Rights Reserved."
            }
        })


    # @commands.slash_command(name="catharsis")
    # async def catharsis(self, ctx, *, message):
    #     """
    #     Emulates CounselingType4 from Game
    #     """

    #     arr2, keywordDict = await self.get_counseling4_db(ctx.locale)
    #     diagnosis, _kw = await self.get_counseling4_diag(message, arr2, keywordDict)
    #     result = await self.get_counseling4_result(ctx.locale, diagnosis)
    #     embed = self.get_counseling4_embed(result)

    #     await ctx.respond(embed=embed)


    @commands.slash_command(name="catharsis")
    async def catharsis(self, ctx):
        # TODO: Import locale to db
        # locale = ctx.locale
        # if ctx.locale not in ('ja', 'ko',):
        #     locale = 'en'

        locale = 'en'

        modal = CatharsisModal(
            cog=self,
            title=self.modal_title.get(locale),
            input_label=self.modal_title.get(locale),
            input_placeholder=self.input_placeholder.get(locale),
        )

        await ctx.send_modal(modal)

    async def modal_callback(self, interaction: discord.Interaction, message: str) -> discord.Embed:
        """
        Emulates CounselingType4 from Game
        """

        # TODO: Import locale to db
        # locale = interaction.locale
        # if ctx.locale not in ('ja', 'ko',):
        #     locale = 'en'

        locale = 'en'
        
        arr2, keywordDict = await self.get_counseling4_db(locale)
        diagnosis, _kw = await self.get_counseling4_diag(message, arr2, keywordDict)
        result = await self.get_counseling4_result(locale, diagnosis)
        embed = self.get_counseling4_embed(result)

        return embed


def setup(bot):
    bot.add_cog(CatharsisCog(bot))
