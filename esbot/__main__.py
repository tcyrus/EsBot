import asyncio
import logging
import os

from .bot import EsBot


async def run():
    bot = EsBot()
    try:
        token = os.getenv('DISCORD_BOT_TOKEN')
        await bot.start(token)
    except KeyboardInterrupt:
        await bot.close()


def main():
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    loop.set_debug(False)
    loop.run_until_complete(run())


if __name__ == '__main__':
    main()
