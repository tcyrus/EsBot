import json
import sqlite3


def main():
    # Load what we need
    tl_sheets = {}
    for locale in ('en', 'ja', 'ko',):
        fname = f'./dump/MonoBehaviour/translation_{locale}.asset.json'
        with open(fname, 'r', encoding='utf8') as f:
            tl_asset = json.load(f)
            tl_sheet = tl_asset['MonoBehaviour']['_sheetDic']['vals']
            tl_sheets[locale] = tl_sheet

    # Some Processing
    # Note: Formatting bug for en BonusMovieDialog/FlavorText
    tl_map = {}
    for locale, sheets in tl_sheets.items():
        lc_map = {}
        for sheet in sheets:
            name = sheet['name']
            data = sheet['data']
            sheet_map = {}
            for item in data:
                key, _, value= item.partition("\t")
                sheet_map[key] = value
            lc_map[name] = sheet_map
        tl_map[locale] = lc_map

    # Flatten for SQLite Execution
    sql_insert = []
    for locale, lc_map in tl_map.items():
        for sheet, sheet_map in lc_map.items():
            for key, value in sheet_map.items():
                sql_insert.append((locale, sheet, key, value,))

    # Create SQLite DB
    # This is just to make the data easier to access later on
    con = sqlite3.connect("./dump/translation.db")
    cur = con.cursor()
    cur.execute("""
    CREATE TABLE translations (
        locale TEXT NOT NULL,
        sheet TEXT NOT NULL,
        key TEXT NOT NULL,
        value TEXT NOT NULL,
        PRIMARY KEY (locale, sheet, key)
    )
    """)
    con.commit()

    cur.executemany("INSERT INTO translations VALUES(?, ?, ?, ?)", sql_insert)
    con.commit()
    con.close()

    # SQLite Vacuum
    # For my piece of mind
    con = sqlite3.connect("./dump/translation.db")
    con.execute("VACUUM")
    con.close()


if __name__ == "__main__":
    main()
