from PIL import Image, ImageFont, ImageDraw

text = "EsBot is the\nBest Bot"
 
def white_speech_bubble(text):
    img = Image.open('../dump/Sprite/1.png')
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype('../dumps/GenEiAntique.ttf', 37)

    w, h = draw.textsize(text, font)

    left = (img.width - w) / 2
    top = 0.32 * img.height - (h / 4)

    draw.text(
        (left, top), text, (0, 0, 0),
        font=font,
        align='center',
        anchor='center'
    )
    return img

def black_speech_bubble(text):
    img = Image.open('../dumps/エスの吹き出し.png')
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype('../dumps/Didot.ttf', 44)

    w, h = draw.textsize(text, font)

    left = (img.width - w) / 2
    top = 0.32 * img.height - (h / 4)

    draw.text(
        (left, top), text, (255, 255, 255),
        font=font,
        align='center',
        anchor='center'
    )
    return img
