# tools

We're assuming that you're following all the legal precautions when extracting assets from the apk.

## Extraction Workflow

- Use apktool for basic extraction
- Use Il2CppDumper to create DummyDll
- Use AssetStudioGUI

## EsBot Talk

Note: AssetStudioGUI is unable to extract these assets properly. I used AssetRipper to get the translation files.

- `MonoBehaviour/translation_en.asset`
- `MonoBehaviour/translation_ja.asset`
- `MonoBehaviour/translation_ko.asset`

Because UnityYAML is weird, none of the Python tools I could find were able to handle it. I ended up converting the files to JSON using Ruby before Processing things in Python.

```
$ ruby -ryaml -rjson -e "puts YAML.load_file('translation_en.asset').to_json" > translation_en.asset.json
```

## White Speech Bubble

- `Sprite/1.png`
- `Font/GenEiAntique.ttf` (en)

## Black Speech Bubble

Note: The Didot font I extracted from game files was corrupted. The font appears to be "Linotype Didot". The TTF file is supposed to contain Regular, Italic, and Bold variations, but only the Regular one appears to be used in-game. 

- `Sprite/エスの吹き出し.png`
- `Font/Didot.ttf` (en)
